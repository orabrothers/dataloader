package com.oracle.nosql.dataloader.configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;


/**
 * This class is for managing access to all configuration in a jvm. <br />
 * Default file is "config.properties". Override this default by setting the
 * property 'config.file' either on the command line or as a system property. <br />
 * <br />
 * The properties in the "config.file" are taken for the values but overrides of
 * those values are allowed via command line or system properties.<br />
 * <br />
 * If the config file does not exist a warning message will be given and only
 * the System.properties are used.
 */
public class Configuration {

    protected static Configuration instance = new Configuration();

    private static final String CONFIG_FILE_NAME = "config.properties";

    private static Properties properties = new Properties();

    static {
        loadFromConfigFile();

        // override the set properties with command line and system properties
        addProperties(System.getProperties());
    }

    /**
     * Remove all configuration properties.
     */
    protected static void clearAllProperties() {
        properties = new Properties();
    }

    //We do this for testability 
    protected static void loadFromConfigFile() {
        String fileName = System.getProperty("config.file", CONFIG_FILE_NAME);
        File file = new File(fileName);
        try (FileReader fileReader = new FileReader(file)) {
            properties.load(fileReader);
        } catch (FileNotFoundException e) {
            warning(fileName);
        } catch (IOException e) {
            warning(fileName);
        }
    }

    //helper function for errors
    private static void warning(String fileName) {
        String warning = "Warning: cannot find file [" + fileName
                + "]. Continuing with only System properties.";
        System.err.println(warning);
    }


    //We do this for testability 
    protected static void addProperties(Properties props) {
        properties.putAll(props);
    }

    // we will extending this class for testability
    protected Configuration() {
        // none shall pass
    }


    /**
     * Returns the instance of the configuration object.
     *
     * @return Configuration
     */
    public static Configuration getInstance() {
        return instance;
    }

    /**
     * Get the String value of the property.
     *
     * @param key
     * @param defaultValue
     * @return The String value of the property that is set or the defaultValue if it is not set.
     */
    public static String getValue(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    /**
     * Get the String values of the property. The values are separated by comma(,).
     *
     * @param key
     * @param defaultValue
     * @return The String values of the property that is set or the defaultValue if it is not set.
     */
    public static String[] getValues(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue).split(",");
    }

    /**
     * Returns an unmodifiable Map of all configuration values that start with the same key prefix.  For example, if
     * prefix is a.b. is specified, properties a.b.c and a.b.c.d will be returned, but not a.c or b.d.
     * <p/>
     * If removePrefix is false, the returned map contains full, prefixed property names as keys, if true, the keys have the
     * prefix removed.
     *
     * @param prefix       the prefix to search
     * @param removePrefix whether the prefix should be removed from keys in the returned Map
     * @return Map of matching configuration keys/values
     */
    public static Map<String, String> getValuesWithPrefix(final String prefix, final boolean removePrefix) {
        final Map<String, String> propertiesWithPrefix = new LinkedHashMap<>();
        for (String key : properties.stringPropertyNames()) {
            if (key.startsWith(prefix)) {
                propertiesWithPrefix.put(removePrefix ? key.substring(prefix.length()) : key, properties.getProperty(key));
            }
        }

        return Collections.unmodifiableMap(propertiesWithPrefix);
    }


    /**
     * Get the int value of a property.
     *
     * @param key
     * @param defaultValue
     * @return The int value of the property that is set or the defaultValue if it is not set.
     * @throws ConfigurationException - if the string does not contain a parsable integer.
     */
    public static int getInt(String key, int defaultValue) throws ConfigurationException {
        //not the most efficient but effective
        String defaultInt = Integer.toString(defaultValue);
        try {
            return Integer.parseInt(properties.getProperty(key, defaultInt));
        } catch (NumberFormatException e) {
            throw new ConfigurationException(e.getMessage());
        }
    }

    /**
     * Get the double value of a property.
     *
     * @param key
     * @param defaultValue
     * @return The double value of the property that is set or the defaultValue if it is not set.
     * @throws ConfigurationException - if the string does not contain a parsable double.
     */
    public static double getDouble(String key, double defaultValue) throws ConfigurationException {
        //not the most efficient but effective
        String defaultDouble = Double.toString(defaultValue);
        try {
            return Double.parseDouble(properties.getProperty(key, defaultDouble));
        } catch (NumberFormatException e) {
            throw new ConfigurationException(e.getMessage());
        }
    }

    /**
     * Get the boolean value of a property.
     *
     * @param key
     * @param defaultValue
     * @return The boolean value of the property that is set or the defaultValue if it is not set.
     * @throws ConfigurationException - if the string does not contain a parsable boolean.
     */
    public static boolean getBoolean(String key, boolean defaultValue) throws ConfigurationException {
        //not the most efficient but effective
        String defaultBoolean = Boolean.toString(defaultValue);
        try {
            return Boolean.parseBoolean(properties.getProperty(key, defaultBoolean));
        } catch (NumberFormatException e) {
            throw new ConfigurationException(e.getMessage());
        }
    }

    /**
     * Get the URL value of a property.
     *
     * @param key
     * @param defaultValue
     * @return the URL value of the property that is set or the defaultValue if it is not set.
     * @throws ConfigurationException - if the string does not contain a parsable URL.
     */
    public static URL getURL(String key, URL defaultValue) throws ConfigurationException {
        //we do this to match the other signature of not tossing exceptions
        //we can revisit this however if this throws an exception so should the other methods.
        try {
            return new URL(properties.getProperty(key, defaultValue.toString()));
        } catch (MalformedURLException e) {
            throw new ConfigurationException(e.getMessage());
        }
    }
}