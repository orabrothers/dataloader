package com.oracle.nosql.dataloader.reader;

import com.oracle.nosql.dataloader.record.CSVRawRecord;
import com.oracle.nosql.dataloader.record.RawRecord;
import com.oracle.nosql.dataloader.record.RawRecordPool;
import oracle.kv.avro.GenericAvroBinding;
import org.apache.avro.Schema;

import java.io.*;
import java.util.Arrays;
import java.util.List;


public class CSVRunnableReader extends RunnableReader {

    private BufferedReader br;
    private Schema schema;
    private GenericAvroBinding genericAvroBinding;
    private int[] primaryKeyPositions;

    public CSVRunnableReader(File file, RawRecordPool rawRecordPool, Schema schema, GenericAvroBinding genericAvroBinding1, String[] primaryKeys) throws FileNotFoundException {
        super(file, rawRecordPool, null, null);
        br = new BufferedReader(new FileReader(file));
        this.schema = schema;
        this.genericAvroBinding = genericAvroBinding1;
        primaryKeyPositions = new int[primaryKeys.length];
        List<Schema.Field> fields = schema.getFields();
        for (int i = 0; i < primaryKeys.length; i++) {
            for (int j = 0; j < fields.size(); j++) {
                if (fields.get(j).name().equals(primaryKeys[i])) {
                    primaryKeyPositions[i] = j;
                    break;
                }
            }
        }
    }

    @Override
    public RawRecord readNext() {
        String line;
        try {
            synchronized (br) {
                line = br.readLine();
            }
            if (line == null) return null;
            String[] columns = line.split(",");
            return new CSVRawRecord(Arrays.asList(columns), schema, genericAvroBinding, primaryKeyPositions);
        } catch (IOException e) {
            return null;
        }
    }
}
