package com.oracle.nosql.dataloader.reader;

import com.oracle.nosql.dataloader.record.RawRecord;
import com.oracle.nosql.dataloader.record.RawRecordPool;
import oracle.kv.avro.AvroBinding;

import java.io.File;

public abstract class RunnableReader implements Runnable {

    protected File file;
    protected RawRecordPool rawRecordPool;
    protected AvroBinding binding;
    protected String[] primaryKeys;

    public RunnableReader(File file, RawRecordPool rawRecordPool, AvroBinding binding, String[] primaryKeys) {
        this.file = file;
        this.rawRecordPool = rawRecordPool;
        this.binding = binding;
        this.primaryKeys = primaryKeys;
    }

    public abstract RawRecord readNext();

    @Override
    public void run() {
        while (true) {
            RawRecord rawRecord = readNext();
            if (!rawRecordPool.inject(rawRecord)) return;
        }
    }
}
