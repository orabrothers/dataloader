package com.oracle.nosql.dataloader;

import com.oracle.nosql.dataloader.configuration.Configuration;
import com.oracle.nosql.dataloader.configuration.ConfigurationException;
import com.oracle.nosql.dataloader.loader.RunnableLoader;
import com.oracle.nosql.dataloader.processer.RunnableProcesser;
import com.oracle.nosql.dataloader.reader.CSVRunnableReader;
import com.oracle.nosql.dataloader.reader.RunnableReader;
import com.oracle.nosql.dataloader.record.KVRecordPool;
import com.oracle.nosql.dataloader.record.RawRecordPool;
import com.oracle.nosql.dataloader.thread.ThreadGroup;
import com.sun.management.OperatingSystemMXBean;
import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.avro.GenericAvroBinding;
import org.apache.avro.Schema;

import java.io.*;
import java.lang.management.ManagementFactory;

public class NoSQLDataLoader {

    KVStore kvStore;

    RunnableReader reader;
    RunnableProcesser processer;
    RunnableLoader loader;

    ThreadGroup readerThreadGroup;
    ThreadGroup processerThreadGroup;
    ThreadGroup loaderThreadGroup;

    RawRecordPool rawRecordPool;
    KVRecordPool kvRecordPool;

    public static void main(String[] args) throws IOException, InterruptedException, ConfigurationException {
        String storeName = Configuration.getValue("kvstore.name", "kvstore");
        String hostName = Configuration.getValue("kvstore.host", "localhost");
        int hostPort = Configuration.getInt("kvstore.host.port", 5000);
        String filePath = Configuration.getValue("file.path", "C:/MOCK_DATA.csv");
        String schemaPath = Configuration.getValue("schema.path", "schema.avsc");
        String[] primaryKeys = Configuration.getValues("primary.keys","id");
        boolean kvstoreAdminAddSchema = Configuration.getBoolean("kvstore.admin.add.schema", true);
        String kvstoreAdminLibPath = Configuration.getValue("kvstore.admin.lib.path", "C:/kv-*/lib");

        long current = System.currentTimeMillis();
        if (kvstoreAdminAddSchema)
            NoSQLDataLoader.addSchemaViaAdminConsole(kvstoreAdminLibPath, hostName, hostPort, schemaPath);
        NoSQLDataLoader noSQLDataLoader = new NoSQLDataLoader(storeName, hostName, hostPort, filePath, schemaPath, primaryKeys);
        noSQLDataLoader.start();
        noSQLDataLoader.optimizeThreads();
        System.out.println("All data is loaded successfully. Used " + (System.currentTimeMillis() - current) / 1000.0 + "s");
    }

    public NoSQLDataLoader(String storeName, String hostName, int hostPort, String filePath, String schemaPath, String[] primaryKeys) throws IOException, InterruptedException, ConfigurationException {

        // Init record pool
        rawRecordPool = new RawRecordPool();
        kvRecordPool = new KVRecordPool();

        // Init schema
        kvStore = KVStoreFactory.getStore(new KVStoreConfig(storeName, hostName + ":" + hostPort));
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(new File(schemaPath));
        
        // Init reader
        File file = new File(filePath);
        String fileName = file.getName();
        String fileSuffix = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
        switch (fileSuffix) {
            case "csv":
                GenericAvroBinding binding = kvStore.getAvroCatalog().getGenericBinding(schema);
                reader = new CSVRunnableReader(file, rawRecordPool, schema, binding, primaryKeys);
                break;
            // TODO: more file types
            default:
                throw new UnsupportedOperationException("The file ." + fileSuffix + " is not supported.");
        }
        // Init reader thread group
        int readerThreadInit = Configuration.getInt("reader.thread.init", 1);
        int readerThreadMax = Configuration.getInt("reader.thread.max", 3);
        int readerThreadPriority = Configuration.getInt("reader.thread.priority", 10);
        readerThreadGroup = new ThreadGroup(reader, readerThreadInit, readerThreadMax, readerThreadPriority);

        // Init processer
        processer = new RunnableProcesser(rawRecordPool, kvRecordPool);
        // Init processer thread group
        int processerThreadInit = Configuration.getInt("processer.thread.init", 1);
        int processerThreadMax = Configuration.getInt("processer.thread.max", 3);
        int processerThreadPriority = Configuration.getInt("processer.thread.priority", 5);
        processerThreadGroup = new ThreadGroup(processer, processerThreadInit, processerThreadMax, processerThreadPriority);

        // Init loader
        loader = new RunnableLoader(kvStore, kvRecordPool);
        // Init loader thread group
        int loaderThreadInit = Configuration.getInt("loader.thread.init", 5);
        int loaderThreadMax = Configuration.getInt("loader.thread.max", 50);
        int loaderThreadPriority = Configuration.getInt("loader.thread.priority", 10);
        loaderThreadGroup = new ThreadGroup(loader, loaderThreadInit, loaderThreadMax, loaderThreadPriority);
    }

    public void start() {
        System.out.println("Loading started.");
        readerThreadGroup.startAll();
        processerThreadGroup.startAll();
        loaderThreadGroup.startAll();

    }

    public static void addSchemaViaAdminConsole(String kvstoreAdminLibPath, String hostName, int hostPort, String schemaPath) throws IOException, InterruptedException {
        System.out.println("Adding schema on server.");
        Runtime.getRuntime().exec(String.format("java -jar kvstore.jar runadmin -host %s -port %s ddl add-schema -file %s/%s", hostName, hostPort, new File(".").getCanonicalPath(), schemaPath), null, new File(kvstoreAdminLibPath)).waitFor();
    }

    public void waitUntilAllFinished() throws InterruptedException {
        readerThreadGroup.joinAll();
        processerThreadGroup.joinAll();
        loaderThreadGroup.joinAll();
    }

    public void optimizeThreads() throws InterruptedException, ConfigurationException {
        int recordPoolMaxDryTimes = Configuration.getInt("record.pool.max.dry.times", 5);
        double cpuUsageLimit = Configuration.getDouble("cpu.usage.limit", 0.9);
        int warmUpTime = Configuration.getInt("optimizer.warm.up.time", 1000);


        int rawRecordPoolDryTimes = 0;
        int kvRecordPoolDryTimes = 0;

        OperatingSystemMXBean osmb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();

        // Lower priority of optimize thread.
        Thread.currentThread().setPriority(Configuration.getInt("optimizer.thread.priority", 1));

        // Warm up
        long currentTimeMillis = System.currentTimeMillis();
        while (System.currentTimeMillis() < currentTimeMillis + warmUpTime) {
            Thread.sleep(0);
        }

        while (!loaderThreadGroup.isAllFinished()) {

            if (rawRecordPool.isDrying()) {
                rawRecordPoolDryTimes++;
            }

            if (kvRecordPool.isDrying()) {
                kvRecordPoolDryTimes++;
            }

            if (rawRecordPoolDryTimes >= recordPoolMaxDryTimes) {
                readerThreadGroup.addOneThread();
                rawRecordPoolDryTimes = 0;
            } else if (kvRecordPoolDryTimes >= recordPoolMaxDryTimes) {
                processerThreadGroup.addOneThread();
                kvRecordPoolDryTimes = 0;
            }

            if (osmb.getProcessCpuLoad() <= cpuUsageLimit)
                loaderThreadGroup.addOneThread();

            // Make other thread to use the cpu time.
            Thread.sleep(0);
        }
    }
}
