package com.oracle.nosql.dataloader.record;

import oracle.kv.Key;
import oracle.kv.Value;

public class KVRecord extends Record {

    private Key key;
    private Value value;

    public KVRecord(Key key, Value value) {
        this.key = key;
        this.value = value;
    }

    public Key getKey() {
        return key;
    }
    
    public Value getValue() {
        return value;
    }
}
