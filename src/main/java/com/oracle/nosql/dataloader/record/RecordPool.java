package com.oracle.nosql.dataloader.record;

import com.oracle.nosql.dataloader.configuration.Configuration;
import com.oracle.nosql.dataloader.configuration.ConfigurationException;

import java.util.concurrent.LinkedBlockingQueue;

public class RecordPool<E extends Record> extends LinkedBlockingQueue<E> {

    private static int POOL_SIZE;
    private static int POOL_DRY_THRESHOLD;
    private static int POOL_INJECT_WAIT_MILLIS;
    private static int POOL_EXTRACT_WAIT_MILLIS;
    protected boolean isInjectionFinished = false;

    static {
        try {
            POOL_SIZE = Configuration.getInt("record.pool.size", 50000);
            POOL_DRY_THRESHOLD = Configuration.getInt("record.pool.dry.threshold", 10000);
            POOL_INJECT_WAIT_MILLIS = Configuration.getInt("record.pool.inject.wait.millis", 1000);
            POOL_EXTRACT_WAIT_MILLIS = Configuration.getInt("record.pool.extract.wait.millis", 100);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    protected RecordPool() {
        super(POOL_SIZE);
    }

    public boolean inject(E record) {
        if (record == null) {
            isInjectionFinished = true;
            return false;
        }
        // Keep trying when the record pool is full.
        while (!offer(record)) {
            try {
                Thread.sleep(POOL_INJECT_WAIT_MILLIS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public E extract() {
        while (true) {
            if (isInjectionFinished && isEmpty()) {
                return null;
            }
            E record = poll();
            if (record == null) {
                try {
                    Thread.sleep(POOL_EXTRACT_WAIT_MILLIS);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                continue;
            }
            return record;
        }
    }

    public boolean isDrying() {
        if (isInjectionFinished)
            return false;
        return size() < POOL_DRY_THRESHOLD;
    }
}
