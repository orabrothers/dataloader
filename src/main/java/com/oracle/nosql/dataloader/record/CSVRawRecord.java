package com.oracle.nosql.dataloader.record;

import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.avro.GenericAvroBinding;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.util.LinkedList;
import java.util.List;

public class CSVRawRecord extends RawRecord {

    private List<String> values;
    private Schema schema;
    private GenericAvroBinding genericAvroBinding;
    private int[] primaryKeyPositions;

    public CSVRawRecord(List<String> values, Schema schema, GenericAvroBinding genericAvroBinding, int[] primaryKeyPositions) {
        super(null, null);
        this.values = values;
        this.schema = schema;
        this.genericAvroBinding = genericAvroBinding;
        this.primaryKeyPositions = primaryKeyPositions;
    }

    @Override
    public KVRecord toKVRecord() {
        return new KVRecord(generateKey(), generateValue());
    }

    private Value generateValue() {
        GenericRecord genericRecord = new GenericData.Record(schema);
        List<Schema.Field> fields = schema.getFields();
        for (int i = 0; i < fields.size(); i++) {
            Schema.Field field = fields.get(i);
            String value;
            try {
                value = values.get(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            switch (field.schema().getName()) {
                case "string":
                    genericRecord.put(field.name(), value);
                    break;
                case "int":
                    Integer intValue;
                    try {
                        intValue = Integer.parseInt(value);
                    } catch (Exception e) {
                        e.printStackTrace();
                        genericRecord.put(field.name(), field.defaultValue().getIntValue());
                        break;
                    }
                    genericRecord.put(field.name(), intValue);
                    break;
                case "double":
                    Double doubleValue;
                    try {
                        doubleValue = Double.parseDouble(value);
                    } catch (Exception e) {
                        e.printStackTrace();
                        genericRecord.put(field.name(), field.defaultValue().getDoubleValue());
                        break;
                    }
                    genericRecord.put(field.name(), doubleValue);
                    break;
                // TODO: more data types
                default:
                    genericRecord.put(field.name(), value);
                    break;
            }
        }
        return genericAvroBinding.toValue(genericRecord);
    }

    private Key generateKey() {
        List<String> keyValues = new LinkedList<>();
        for (int primaryKeyPosition : primaryKeyPositions) {
            keyValues.add(values.get(primaryKeyPosition));
        }
        return Key.createKey(keyValues);
    }
}
