package com.oracle.nosql.dataloader.record;

import oracle.kv.avro.AvroBinding;

public abstract class RawRecord extends Record {

    protected AvroBinding binding;
    protected String[] primaryKeys;

    public RawRecord(AvroBinding binding, String[] primaryKeys) {
        this.binding = binding;
        this.primaryKeys = primaryKeys;
    }
    
    public abstract KVRecord toKVRecord();

}
