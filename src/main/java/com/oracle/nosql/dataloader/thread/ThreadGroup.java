package com.oracle.nosql.dataloader.thread;

import java.util.LinkedList;
import java.util.List;

public class ThreadGroup {

    private int max;
    private int priority;

    private List<Thread> threads = new LinkedList<>();

    private Runnable target;

    public ThreadGroup(Runnable target, int init, int max, int priority) {
        this.max = max;
        this.priority = priority;
        this.target = target;
        for (int i = 0; i < init; i++) {
            Thread thread = new Thread(target);
            thread.setPriority(priority);
            threads.add(thread);
        }
    }

    public boolean addOneThread() {
        if (threads.size() >= max) {
            return false;
        }

        Thread thread = new Thread(target);
        thread.setPriority(priority);
        threads.add(thread);
        thread.start();

        System.out.println(target.getClass().getName() + " used " + threads.size() + " threads.");

        return true;
    }

    public void startAll() {
        for (Thread thread : threads) {
            thread.start();
        }
        System.out.println(target.getClass().getCanonicalName() + " used " + threads.size() + " threads.");
    }

    public void joinAll() throws InterruptedException {
        for (Thread thread : threads) {
            thread.join();
        }
    }

    public boolean isAllFinished() {
        for (Thread thread : threads) {
            if (thread.getState() != Thread.State.TERMINATED)
                return false;
        }
        return true;
    }
}
