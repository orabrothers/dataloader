package com.oracle.nosql.dataloader.processer;

import com.oracle.nosql.dataloader.record.KVRecord;
import com.oracle.nosql.dataloader.record.KVRecordPool;
import com.oracle.nosql.dataloader.record.RawRecord;
import com.oracle.nosql.dataloader.record.RawRecordPool;
import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.avro.GenericAvroBinding;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.util.List;

public class RunnableProcesser implements Runnable {

    private RawRecordPool rawRecordPool;
    private KVRecordPool kvRecordPool;

    public RunnableProcesser(RawRecordPool rawRecordPool, KVRecordPool kvRecordPool) {
        this.rawRecordPool = rawRecordPool;
        this.kvRecordPool = kvRecordPool;
    }

    public KVRecord rawToKV(RawRecord rawRecord) {
        return rawRecord.toKVRecord();
    }

    
    @Override
    public void run() {
        while (true) {
            RawRecord rawRecord = rawRecordPool.extract();
            if (rawRecord == null) {
                kvRecordPool.inject(null);
                return;
            }
            if (!RecordFilter.filter(rawRecord)) continue;
            kvRecordPool.inject(rawToKV(rawRecord));
        }
    }
}
