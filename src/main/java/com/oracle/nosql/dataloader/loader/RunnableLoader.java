package com.oracle.nosql.dataloader.loader;

import com.oracle.nosql.dataloader.record.KVRecord;
import com.oracle.nosql.dataloader.record.KVRecordPool;
import oracle.kv.KVStore;

public class RunnableLoader implements Runnable {

    private KVStore store;
    private KVRecordPool kvRecordPool;

    public RunnableLoader(KVStore store, KVRecordPool kvRecordPool) {
        this.store = store;
        this.kvRecordPool = kvRecordPool;
    }

    private void load(KVRecord kvRecord) {
        store.put(kvRecord.getKey(), kvRecord.getValue());
    }

    @Override
    public void run() {
        while (true) {
            KVRecord kvRecord = kvRecordPool.extract();
            if (kvRecord == null) return;
            load(kvRecord);
        }
    }
}
